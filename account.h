

#ifndef ACCOUNT_H
#define ACCOUNT_H

//#include "apstring.h"

#include <string>

using namespace std;

class account
{

	public:
		//constructors

		account();
		account(const string &password, double balance);
		account(const account &a);

		// accessor

		double getBalance(const string &password) const;

		// modifiers
		
		int setPassword(const string &password, const string newPassword);
		double deposit(const string &password, double amount);
		double withdraw(const string &password, double amount);

		//assignment

		const account& operator = (const account &a);

	private:

		// data member
		
		string myPassword;
		double myBalance;

};

account::account()
{

	myPassword = "";
	myBalance = 0.00;

}

account::account(const string &password, double balance)
{

	myPassword = password;
	myBalance = balance;

}

account::account(const account &a)
{

	myPassword = a.myPassword;
	myBalance = a.myBalance;

}

double account::getBalance(const string &password)const
{

	if (password == myPassword)
		return myBalance;
	else
		return -1;

}

double account::deposit(const string &password, double amount)
{

	if (password == myPassword){
		myBalance = myBalance + amount;
		return myBalance;
	}else{
		return -1;
	}
}

double account::withdraw(const string &password, double amount)
{

	if (password == myPassword)
		if ((amount >= 0) && (amount <= myBalance))
		{
			myBalance = myBalance - amount;
			return myBalance;
		}
		else
			return -2;
	else
		return -1;

}

const account& account::operator = (const account &a)
{

	if (this != &a)
	{
		myPassword = a.myPassword;
		myBalance = a.myBalance;
	}
	return *this;

}

#define ACCOUNT_H
#endif
