
#include <iostream>
#include <iomanip>

#include "account.h"

using namespace std;

int main(){

	account judy("beelzebub", 50.00);
	account jim("gadzooks", 100.00);
	account target;

	cout <<setiosflags(ios::fixed | ios::showpoint)<<setprecision(2);

	cout <<"Judy's balance = $"
		<<judy.getBalance("beelzebub") <<endl;
	cout <<"Jim's balance = $"
		<<jim.getBalance("gadzooks") <<endl;

	cout <<"Result of invalid password ="
		<<jim.getBalance("rosebud") <<endl;

	cout <<"Depositing $20.00 to Jim, new balance = $"
		<<jim.deposit("gadzooks", 20.00)<<endl;

	cout <<"Result of overdraft from Judy ($51.00) ="
		<<judy.withdraw("beelzebub", 51.00)<<endl;

	target = judy;
	cout <<"Target's blance = $"
		<<target.getBalance("beelzebub")<<endl;
	return 0;

 
}

